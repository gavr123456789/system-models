using Math;

class Trud {
	public uint   trudoemkost;
	StringBuilder file_content;
	public Trud(){
		file_content = new StringBuilder();
	}
	public void inc(){
		trudoemkost++;
	}
	public void save_data(double t){
		file_content.append("(" + t.to_string() + ";" + trudoemkost.to_string() + ") ");
	}
	public void to_file(){
		FileUtils.set_contents("Трудоемкость.txt", file_content.str);
	}	
}


class MainMath {
	bool test_trudoemkost;
	bool test_tochnost;
	double tochnost = 0;
    double p = 2 * 100000;
    double a = 1.1;
    double m = 1000;
    double u = 10;
    double cx = 0.2;
    double cy = 0.05;
    double m1 = 0.03;
    double m2 = 0.06;
    double T  = 13; // T
    double t  = 0;
    double x1 = 1000;
    double x2 = 0.5;
    double x3 = 0;
    double x4 = 0;
	double x5 = 0.5;
	double g = 9.81;
	double h = 0;

	double z1=0;
	double z2=0;
	double z3=0;
	double z4=0;
	double z5=0;

	//double[] arr1 = {};
	//double[] arr2 = {};
	//double[] arr3 = {};
	double[] arr4 = {};

	StringBuilder trud_graph;
	StringBuilder tochnost_graph;
	Trud trud;

	public MainMath(bool test_trudoemkost = false, bool test_tochnost = false){
		trud = new Trud();
		trud_graph = new StringBuilder();
		tochnost_graph = new StringBuilder();
		this.test_tochnost = test_tochnost;
		this.test_trudoemkost = test_trudoemkost;
	}

	public int main_math(){

		if(!test_trudoemkost && !test_trudoemkost ){
			stdout.printf ("Введите h: ");
			h = double.parse (stdin.read_line ());
			if (h < 0.0000005) {stdout.printf ("вы дурак"); return -1;}
			t = h;
			math();
		}
		
		if(test_tochnost){
			for (h = 0.00001; h <= 0.1; h*=10) {
				t = h;
				math();
				trud_graph.append("(" + h.to_string() + ";" + trud.trudoemkost.to_string()  + ")\n");
				tochnost_graph.append("(" + h.to_string() + ";" +  tochnost.to_string() + ")\n");
			}
			sostavit_otchet_tochnost();
			sostavit_otchet_trudoemkost();
		}

		return 0;
	}

	public void sostavit_otchet_tochnost(){
		FileUtils.set_contents("Трудоемкость_от_шага.txt", trud_graph.str);		
	}
	public void sostavit_otchet_trudoemkost(){
		FileUtils.set_contents("Погрешность_от_шага.txt", tochnost_graph.str);
	}

	void math(){
		var builder1 = new StringBuilder();
		var builder2 = new StringBuilder();
		var builder3 = new StringBuilder();
		var builder4 = new StringBuilder();
		var builder5 = new StringBuilder();
		int generator  = 0;// чтобы ставить ентер через каждый третий шаг
		int generator2 = 0;// чтобы брать только каждое 1000
		bool tochnost_flag = true;
		
		//int j = 0;// итератор по массивам arrn
		do {
			
			for (t = 0; t < T;   t += h, generator2++, m++, trud.inc()) {
	
				double formula1 = -g * sin(x2) + ((p-a*cx * x1 * x1)/(m-u*t));
				double formula2 = (-g+(p*sin(x5-x2)+a*cy*x1*x1)/(m-u*t))/x1;
				double formula3 = (m1*a*x1*x1*(x2-x5)-m2*a*x1*x1*x3)/(m-u*t);
				double formula4 = x1*sin(x2);
				double formula5 = x3;
	
				z1 = z1 + formula1 * h;
				z2 = z2 + formula2 * h;
				z3 = z3 + formula3 * h;
				z4 = z4 + formula4 * h; 
				z5 = z5 + formula5 * h;
	
				if (generator2 == 5000){ 
					generator++;
					generator2 = 0;
					
					//  arr1 += z1;
					//  arr2[j] = z2;
					//  arr3[j] = z3;
					arr4 += z4;
					//  arr5[j] = z5;
					if(!test_trudoemkost && !test_trudoemkost ){
						builder1.append ("(" + t.to_string() + ";" + z1.to_string() + ") ");
						builder2.append ("(" + t.to_string() + ";" + z2.to_string() + ") ");
						builder3.append ("(" + t.to_string() + ";" + z3.to_string() + ") ");
						builder4.append ("(" + t.to_string() + ";" + z4.to_string() + ") ");
						builder5.append ("(" + t.to_string() + ";" + z5.to_string() + ") ");
						trud.save_data(t);
	
						if (generator == 3) { // деление по строкам 
							generator = 0;
							builder1.append("\n"); 
							builder2.append("\n");
							builder3.append("\n");
							builder4.append("\n"); 
							builder5.append("\n"); 
						}
					}
				}
				tochnost_flag = false;
			}
	
			if(tochnost_check (arr4[arr4.length-2], arr4[arr4.length-1]) > 1 && !test_tochnost){
				stdout.printf ("Заданный шаг не позволяет определить X4 с точностью 1%% !, Шаг будет подобран автоматически");
				h /= 2;
				tochnost_flag = true;
			}
	
		} while (tochnost_flag);
		//message (@"arr4-1 $(arr4[1000-1]) arr4-2 $(arr4[1000-2])");
		tochnost = tochnost_check (arr4[arr4.length-2], arr4[arr4.length-1]); //fabs((arr4[arr4.length-2] - arr4[arr4.length-1])/arr4[arr4.length-1])*100;
		
		stdout.printf (@"Точность = $tochnost\n");
		if(!test_trudoemkost && !test_trudoemkost){
			FileUtils.set_contents ("1.txt", builder1.str);
			FileUtils.set_contents ("2.txt", builder2.str);
			FileUtils.set_contents ("3.txt", builder3.str);
			FileUtils.set_contents ("4.txt", builder4.str);
			FileUtils.set_contents ("5.txt", builder5.str);
			trud.to_file();
		}
		
	}
}

static int main(string[] args) {
	var sas2 = new MainMath(true, true);
	sas2.main_math();
    return 0;
}

double tochnost_check(double a, double b){
	return fabs((a - b) / b)*100;
}