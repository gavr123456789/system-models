using Gtk;
[GtkTemplate (ui = "/org/gavr/calc/calc.ui")]
public class View : Window {

	[GtkChild]
	Grid mainGrid;
	
	[GtkChild]
    public ListBox history;
	
	LabelGrid lg;
	Model model;
	Label[,] labels;

	string old_l_txt;
	string l_txt;

	
	public View () {
		model  = new Model();
		lg = new LabelGrid();
		labels = {
				  	{lg.zx11, lg.zx21, lg.zx31, lg.zx41, lg.zx51},
				  	{lg.zx12, lg.zx22, lg.zx32, lg.zx42, lg.zx52},
				  	{lg.zx13, lg.zx23, lg.zx33, lg.zx43, lg.zx53},
				  	{lg.zx14, lg.zx24, lg.zx34, lg.zx44, lg.zx54},
				};
		mainGrid.attach(lg,1,2,5,4);
		//установка первого состояния визуально
		l_txt = labels[model.pos.y-1, model.pos.x-1].label;
		labels[model.pos.x-1,model.pos.x-1].set_markup(@"<b><big><big>$l_txt</big></big></b>");
		this.show_all();
		this.destroy.connect (Gtk.main_quit);
	}

	[GtkCallback]
	private void input (Button button) {
		//string btn_txt = ;
		int a = int.parse(button.label.get_char(1).to_string());
		message(a.to_string());
		model.input(a);

		//уменьшение предыдущей
		message(labels[model.old_pos.y-1, model.old_pos.x-1].label);
		labels[model.old_pos.y-1, model.old_pos.x-1].set_markup(@"$(model.z[model.old_pos.x-1, model.old_pos.y-1].to_string())");

		//увеличение следующей
		labels[ model.pos.y-1,model.pos.x-1].set_markup(@"<b><big><big>$(model.z[model.pos.x-1, model.pos.y-1].to_string())</big></big></b>");
	
		var label1 = new Label(@"Выход: $(model.output_check())");
		history.insert(label1,-1);
		label1.show_all();
	}
}

void main(string[] args) {
	Gtk.init (ref args);
	var widget = new View ();
	//widget.model.main_loop();
	Gtk.main ();
}
