/* calc.c generated by valac 0.46.2, the Vala compiler
 * generated from calc.vala, do not modify */

#include <gtk/gtk.h>
#include <glib-object.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>

#define TYPE_VIEW (view_get_type ())
#define VIEW(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_VIEW, View))
#define VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_VIEW, ViewClass))
#define IS_VIEW(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_VIEW))
#define IS_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_VIEW))
#define VIEW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_VIEW, ViewClass))

typedef struct _View View;
typedef struct _ViewClass ViewClass;
typedef struct _ViewPrivate ViewPrivate;

#define TYPE_LABEL_GRID (label_grid_get_type ())
#define LABEL_GRID(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_LABEL_GRID, LabelGrid))
#define LABEL_GRID_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_LABEL_GRID, LabelGridClass))
#define IS_LABEL_GRID(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_LABEL_GRID))
#define IS_LABEL_GRID_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_LABEL_GRID))
#define LABEL_GRID_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_LABEL_GRID, LabelGridClass))

typedef struct _LabelGrid LabelGrid;
typedef struct _LabelGridClass LabelGridClass;

#define TYPE_MODEL (model_get_type ())
#define MODEL(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MODEL, Model))
#define MODEL_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MODEL, ModelClass))
#define IS_MODEL(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MODEL))
#define IS_MODEL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MODEL))
#define MODEL_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MODEL, ModelClass))

typedef struct _Model Model;
typedef struct _ModelClass ModelClass;
enum  {
	VIEW_0_PROPERTY,
	VIEW_NUM_PROPERTIES
};
static GParamSpec* view_properties[VIEW_NUM_PROPERTIES];
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))
#define _g_free0(var) (var = (g_free (var), NULL))
typedef struct _LabelGridPrivate LabelGridPrivate;
typedef struct _ModelPrivate ModelPrivate;

#define TYPE_POS (pos_get_type ())
typedef struct _Pos Pos;

struct _View {
	GtkWindow parent_instance;
	ViewPrivate * priv;
	GtkListBox* history;
};

struct _ViewClass {
	GtkWindowClass parent_class;
};

struct _ViewPrivate {
	GtkGrid* mainGrid;
	LabelGrid* lg;
	Model* model;
	GtkLabel** labels;
	gint labels_length1;
	gint labels_length2;
	gchar* old_l_txt;
	gchar* l_txt;
};

struct _LabelGrid {
	GtkGrid parent_instance;
	LabelGridPrivate * priv;
	GtkLabel* zx11;
	GtkLabel* zx21;
	GtkLabel* zx31;
	GtkLabel* zx41;
	GtkLabel* zx51;
	GtkLabel* zx12;
	GtkLabel* zx22;
	GtkLabel* zx32;
	GtkLabel* zx42;
	GtkLabel* zx52;
	GtkLabel* zx13;
	GtkLabel* zx23;
	GtkLabel* zx33;
	GtkLabel* zx43;
	GtkLabel* zx53;
	GtkLabel* zx14;
	GtkLabel* zx24;
	GtkLabel* zx34;
	GtkLabel* zx44;
	GtkLabel* zx54;
};

struct _LabelGridClass {
	GtkGridClass parent_class;
};

struct _Pos {
	gint x;
	gint y;
};

struct _Model {
	GObject parent_instance;
	ModelPrivate * priv;
	Pos pos;
	Pos old_pos;
	gint* z;
	gint z_length1;
	gint z_length2;
	gboolean* y;
	gint y_length1;
	gint y_length2;
};

struct _ModelClass {
	GObjectClass parent_class;
};

static gint View_private_offset;
static gpointer view_parent_class = NULL;

GType view_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (View, g_object_unref)
GType label_grid_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (LabelGrid, g_object_unref)
GType model_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (Model, g_object_unref)
View* view_new (void);
View* view_construct (GType object_type);
Model* model_new (void);
Model* model_construct (GType object_type);
LabelGrid* label_grid_new (void);
LabelGrid* label_grid_construct (GType object_type);
GType pos_get_type (void) G_GNUC_CONST;
Pos* pos_dup (const Pos* self);
void pos_free (Pos* self);
static void _gtk_main_quit_gtk_widget_destroy (GtkWidget* _sender,
                                        gpointer self);
static void view_input (View* self,
                 GtkButton* button);
void model_input (Model* self,
                  gint g);
gint model_output_check (Model* self);
static void _view_input_gtk_button_clicked (GtkButton* _sender,
                                     gpointer self);
static void view_finalize (GObject * obj);
void _vala_main (gchar** args,
                 gint args_length1);
static void _vala_array_destroy (gpointer array,
                          gint array_length,
                          GDestroyNotify destroy_func);
static void _vala_array_free (gpointer array,
                       gint array_length,
                       GDestroyNotify destroy_func);

static inline gpointer
view_get_instance_private (View* self)
{
	return G_STRUCT_MEMBER_P (self, View_private_offset);
}

static gpointer
_g_object_ref0 (gpointer self)
{
#line 22 "../calc.vala"
	return self ? g_object_ref (self) : NULL;
#line 174 "calc.c"
}

static const gchar*
string_to_string (const gchar* self)
{
	const gchar* result = NULL;
#line 1516 "glib-2.0.vapi"
	g_return_val_if_fail (self != NULL, NULL);
#line 1517 "glib-2.0.vapi"
	result = self;
#line 1517 "glib-2.0.vapi"
	return result;
#line 187 "calc.c"
}

static void
_gtk_main_quit_gtk_widget_destroy (GtkWidget* _sender,
                                   gpointer self)
{
#line 33 "../calc.vala"
	gtk_main_quit ();
#line 196 "calc.c"
}

View*
view_construct (GType object_type)
{
	View * self = NULL;
	Model* _tmp0_;
	LabelGrid* _tmp1_;
	LabelGrid* _tmp2_;
	GtkLabel* _tmp3_;
	GtkLabel* _tmp4_;
	LabelGrid* _tmp5_;
	GtkLabel* _tmp6_;
	GtkLabel* _tmp7_;
	LabelGrid* _tmp8_;
	GtkLabel* _tmp9_;
	GtkLabel* _tmp10_;
	LabelGrid* _tmp11_;
	GtkLabel* _tmp12_;
	GtkLabel* _tmp13_;
	LabelGrid* _tmp14_;
	GtkLabel* _tmp15_;
	GtkLabel* _tmp16_;
	LabelGrid* _tmp17_;
	GtkLabel* _tmp18_;
	GtkLabel* _tmp19_;
	LabelGrid* _tmp20_;
	GtkLabel* _tmp21_;
	GtkLabel* _tmp22_;
	LabelGrid* _tmp23_;
	GtkLabel* _tmp24_;
	GtkLabel* _tmp25_;
	LabelGrid* _tmp26_;
	GtkLabel* _tmp27_;
	GtkLabel* _tmp28_;
	LabelGrid* _tmp29_;
	GtkLabel* _tmp30_;
	GtkLabel* _tmp31_;
	LabelGrid* _tmp32_;
	GtkLabel* _tmp33_;
	GtkLabel* _tmp34_;
	LabelGrid* _tmp35_;
	GtkLabel* _tmp36_;
	GtkLabel* _tmp37_;
	LabelGrid* _tmp38_;
	GtkLabel* _tmp39_;
	GtkLabel* _tmp40_;
	LabelGrid* _tmp41_;
	GtkLabel* _tmp42_;
	GtkLabel* _tmp43_;
	LabelGrid* _tmp44_;
	GtkLabel* _tmp45_;
	GtkLabel* _tmp46_;
	LabelGrid* _tmp47_;
	GtkLabel* _tmp48_;
	GtkLabel* _tmp49_;
	LabelGrid* _tmp50_;
	GtkLabel* _tmp51_;
	GtkLabel* _tmp52_;
	LabelGrid* _tmp53_;
	GtkLabel* _tmp54_;
	GtkLabel* _tmp55_;
	LabelGrid* _tmp56_;
	GtkLabel* _tmp57_;
	GtkLabel* _tmp58_;
	LabelGrid* _tmp59_;
	GtkLabel* _tmp60_;
	GtkLabel* _tmp61_;
	GtkLabel** _tmp62_;
	GtkGrid* _tmp63_;
	LabelGrid* _tmp64_;
	GtkLabel** _tmp65_;
	gint _tmp65__length1;
	gint _tmp65__length2;
	Model* _tmp66_;
	Pos _tmp67_;
	Model* _tmp68_;
	Pos _tmp69_;
	GtkLabel* _tmp70_;
	const gchar* _tmp71_;
	const gchar* _tmp72_;
	gchar* _tmp73_;
	GtkLabel** _tmp74_;
	gint _tmp74__length1;
	gint _tmp74__length2;
	Model* _tmp75_;
	Pos _tmp76_;
	Model* _tmp77_;
	Pos _tmp78_;
	GtkLabel* _tmp79_;
	const gchar* _tmp80_;
	const gchar* _tmp81_;
	gchar* _tmp82_;
	gchar* _tmp83_;
#line 19 "../calc.vala"
	self = (View*) g_object_new (object_type, NULL);
#line 20 "../calc.vala"
	_tmp0_ = model_new ();
#line 20 "../calc.vala"
	_g_object_unref0 (self->priv->model);
#line 20 "../calc.vala"
	self->priv->model = _tmp0_;
#line 21 "../calc.vala"
	_tmp1_ = label_grid_new ();
#line 21 "../calc.vala"
	g_object_ref_sink (_tmp1_);
#line 21 "../calc.vala"
	_g_object_unref0 (self->priv->lg);
#line 21 "../calc.vala"
	self->priv->lg = _tmp1_;
#line 22 "../calc.vala"
	_tmp2_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp3_ = _tmp2_->zx11;
#line 22 "../calc.vala"
	_tmp4_ = _g_object_ref0 (_tmp3_);
#line 22 "../calc.vala"
	_tmp5_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp6_ = _tmp5_->zx21;
#line 22 "../calc.vala"
	_tmp7_ = _g_object_ref0 (_tmp6_);
#line 22 "../calc.vala"
	_tmp8_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp9_ = _tmp8_->zx31;
#line 22 "../calc.vala"
	_tmp10_ = _g_object_ref0 (_tmp9_);
#line 22 "../calc.vala"
	_tmp11_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp12_ = _tmp11_->zx41;
#line 22 "../calc.vala"
	_tmp13_ = _g_object_ref0 (_tmp12_);
#line 22 "../calc.vala"
	_tmp14_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp15_ = _tmp14_->zx51;
#line 22 "../calc.vala"
	_tmp16_ = _g_object_ref0 (_tmp15_);
#line 22 "../calc.vala"
	_tmp17_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp18_ = _tmp17_->zx12;
#line 22 "../calc.vala"
	_tmp19_ = _g_object_ref0 (_tmp18_);
#line 22 "../calc.vala"
	_tmp20_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp21_ = _tmp20_->zx22;
#line 22 "../calc.vala"
	_tmp22_ = _g_object_ref0 (_tmp21_);
#line 22 "../calc.vala"
	_tmp23_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp24_ = _tmp23_->zx32;
#line 22 "../calc.vala"
	_tmp25_ = _g_object_ref0 (_tmp24_);
#line 22 "../calc.vala"
	_tmp26_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp27_ = _tmp26_->zx42;
#line 22 "../calc.vala"
	_tmp28_ = _g_object_ref0 (_tmp27_);
#line 22 "../calc.vala"
	_tmp29_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp30_ = _tmp29_->zx52;
#line 22 "../calc.vala"
	_tmp31_ = _g_object_ref0 (_tmp30_);
#line 22 "../calc.vala"
	_tmp32_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp33_ = _tmp32_->zx13;
#line 22 "../calc.vala"
	_tmp34_ = _g_object_ref0 (_tmp33_);
#line 22 "../calc.vala"
	_tmp35_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp36_ = _tmp35_->zx23;
#line 22 "../calc.vala"
	_tmp37_ = _g_object_ref0 (_tmp36_);
#line 22 "../calc.vala"
	_tmp38_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp39_ = _tmp38_->zx33;
#line 22 "../calc.vala"
	_tmp40_ = _g_object_ref0 (_tmp39_);
#line 22 "../calc.vala"
	_tmp41_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp42_ = _tmp41_->zx43;
#line 22 "../calc.vala"
	_tmp43_ = _g_object_ref0 (_tmp42_);
#line 22 "../calc.vala"
	_tmp44_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp45_ = _tmp44_->zx53;
#line 22 "../calc.vala"
	_tmp46_ = _g_object_ref0 (_tmp45_);
#line 22 "../calc.vala"
	_tmp47_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp48_ = _tmp47_->zx14;
#line 22 "../calc.vala"
	_tmp49_ = _g_object_ref0 (_tmp48_);
#line 22 "../calc.vala"
	_tmp50_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp51_ = _tmp50_->zx24;
#line 22 "../calc.vala"
	_tmp52_ = _g_object_ref0 (_tmp51_);
#line 22 "../calc.vala"
	_tmp53_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp54_ = _tmp53_->zx34;
#line 22 "../calc.vala"
	_tmp55_ = _g_object_ref0 (_tmp54_);
#line 22 "../calc.vala"
	_tmp56_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp57_ = _tmp56_->zx44;
#line 22 "../calc.vala"
	_tmp58_ = _g_object_ref0 (_tmp57_);
#line 22 "../calc.vala"
	_tmp59_ = self->priv->lg;
#line 22 "../calc.vala"
	_tmp60_ = _tmp59_->zx54;
#line 22 "../calc.vala"
	_tmp61_ = _g_object_ref0 (_tmp60_);
#line 22 "../calc.vala"
	_tmp62_ = g_new0 (GtkLabel*, (4 * 5) + 1);
#line 22 "../calc.vala"
	_tmp62_[0] = _tmp4_;
#line 22 "../calc.vala"
	_tmp62_[1] = _tmp7_;
#line 22 "../calc.vala"
	_tmp62_[2] = _tmp10_;
#line 22 "../calc.vala"
	_tmp62_[3] = _tmp13_;
#line 22 "../calc.vala"
	_tmp62_[4] = _tmp16_;
#line 22 "../calc.vala"
	_tmp62_[5] = _tmp19_;
#line 22 "../calc.vala"
	_tmp62_[6] = _tmp22_;
#line 22 "../calc.vala"
	_tmp62_[7] = _tmp25_;
#line 22 "../calc.vala"
	_tmp62_[8] = _tmp28_;
#line 22 "../calc.vala"
	_tmp62_[9] = _tmp31_;
#line 22 "../calc.vala"
	_tmp62_[10] = _tmp34_;
#line 22 "../calc.vala"
	_tmp62_[11] = _tmp37_;
#line 22 "../calc.vala"
	_tmp62_[12] = _tmp40_;
#line 22 "../calc.vala"
	_tmp62_[13] = _tmp43_;
#line 22 "../calc.vala"
	_tmp62_[14] = _tmp46_;
#line 22 "../calc.vala"
	_tmp62_[15] = _tmp49_;
#line 22 "../calc.vala"
	_tmp62_[16] = _tmp52_;
#line 22 "../calc.vala"
	_tmp62_[17] = _tmp55_;
#line 22 "../calc.vala"
	_tmp62_[18] = _tmp58_;
#line 22 "../calc.vala"
	_tmp62_[19] = _tmp61_;
#line 22 "../calc.vala"
	self->priv->labels = (_vala_array_free (self->priv->labels, self->priv->labels_length1 * self->priv->labels_length2, (GDestroyNotify) g_object_unref), NULL);
#line 22 "../calc.vala"
	self->priv->labels = _tmp62_;
#line 22 "../calc.vala"
	self->priv->labels_length1 = 4;
#line 22 "../calc.vala"
	self->priv->labels_length2 = 5;
#line 28 "../calc.vala"
	_tmp63_ = self->priv->mainGrid;
#line 28 "../calc.vala"
	_tmp64_ = self->priv->lg;
#line 28 "../calc.vala"
	gtk_grid_attach (_tmp63_, (GtkWidget*) _tmp64_, 1, 2, 5, 4);
#line 30 "../calc.vala"
	_tmp65_ = self->priv->labels;
#line 30 "../calc.vala"
	_tmp65__length1 = self->priv->labels_length1;
#line 30 "../calc.vala"
	_tmp65__length2 = self->priv->labels_length2;
#line 30 "../calc.vala"
	_tmp66_ = self->priv->model;
#line 30 "../calc.vala"
	_tmp67_ = _tmp66_->pos;
#line 30 "../calc.vala"
	_tmp68_ = self->priv->model;
#line 30 "../calc.vala"
	_tmp69_ = _tmp68_->pos;
#line 30 "../calc.vala"
	_tmp70_ = _tmp65_[((_tmp67_.y - 1) * _tmp65__length2) + (_tmp69_.x - 1)];
#line 30 "../calc.vala"
	_tmp71_ = gtk_label_get_label (_tmp70_);
#line 30 "../calc.vala"
	_tmp72_ = _tmp71_;
#line 30 "../calc.vala"
	_tmp73_ = g_strdup (_tmp72_);
#line 30 "../calc.vala"
	_g_free0 (self->priv->l_txt);
#line 30 "../calc.vala"
	self->priv->l_txt = _tmp73_;
#line 31 "../calc.vala"
	_tmp74_ = self->priv->labels;
#line 31 "../calc.vala"
	_tmp74__length1 = self->priv->labels_length1;
#line 31 "../calc.vala"
	_tmp74__length2 = self->priv->labels_length2;
#line 31 "../calc.vala"
	_tmp75_ = self->priv->model;
#line 31 "../calc.vala"
	_tmp76_ = _tmp75_->pos;
#line 31 "../calc.vala"
	_tmp77_ = self->priv->model;
#line 31 "../calc.vala"
	_tmp78_ = _tmp77_->pos;
#line 31 "../calc.vala"
	_tmp79_ = _tmp74_[((_tmp76_.x - 1) * _tmp74__length2) + (_tmp78_.x - 1)];
#line 31 "../calc.vala"
	_tmp80_ = self->priv->l_txt;
#line 31 "../calc.vala"
	_tmp81_ = string_to_string (_tmp80_);
#line 31 "../calc.vala"
	_tmp82_ = g_strconcat ("<b><big><big>", _tmp81_, "</big></big></b>", NULL);
#line 31 "../calc.vala"
	_tmp83_ = _tmp82_;
#line 31 "../calc.vala"
	gtk_label_set_markup (_tmp79_, _tmp83_);
#line 31 "../calc.vala"
	_g_free0 (_tmp83_);
#line 32 "../calc.vala"
	gtk_widget_show_all ((GtkWidget*) self);
#line 33 "../calc.vala"
	g_signal_connect ((GtkWidget*) self, "destroy", (GCallback) _gtk_main_quit_gtk_widget_destroy, NULL);
#line 19 "../calc.vala"
	return self;
#line 543 "calc.c"
}

View*
view_new (void)
{
#line 19 "../calc.vala"
	return view_construct (TYPE_VIEW);
#line 551 "calc.c"
}

static gunichar
string_get_char (const gchar* self,
                 glong index)
{
	gunichar result = 0U;
#line 1210 "glib-2.0.vapi"
	g_return_val_if_fail (self != NULL, 0U);
#line 1211 "glib-2.0.vapi"
	result = g_utf8_get_char (((gchar*) self) + index);
#line 1211 "glib-2.0.vapi"
	return result;
#line 565 "calc.c"
}

static gchar*
g_unichar_to_string (gunichar self)
{
	gchar* str = NULL;
	gchar* _tmp0_;
	gchar* result = NULL;
#line 1019 "glib-2.0.vapi"
	_tmp0_ = g_new0 (gchar, 7);
#line 1019 "glib-2.0.vapi"
	str = (gchar*) _tmp0_;
#line 1020 "glib-2.0.vapi"
	g_unichar_to_utf8 (self, str);
#line 1021 "glib-2.0.vapi"
	result = str;
#line 1021 "glib-2.0.vapi"
	return result;
#line 584 "calc.c"
}

static void
view_input (View* self,
            GtkButton* button)
{
	gint a = 0;
	const gchar* _tmp0_;
	const gchar* _tmp1_;
	gchar* _tmp2_;
	gchar* _tmp3_;
	gint _tmp4_;
	gchar* _tmp5_;
	gchar* _tmp6_;
	Model* _tmp7_;
	GtkLabel** _tmp8_;
	gint _tmp8__length1;
	gint _tmp8__length2;
	Model* _tmp9_;
	Pos _tmp10_;
	Model* _tmp11_;
	Pos _tmp12_;
	GtkLabel* _tmp13_;
	const gchar* _tmp14_;
	const gchar* _tmp15_;
	GtkLabel** _tmp16_;
	gint _tmp16__length1;
	gint _tmp16__length2;
	Model* _tmp17_;
	Pos _tmp18_;
	Model* _tmp19_;
	Pos _tmp20_;
	GtkLabel* _tmp21_;
	Model* _tmp22_;
	gint* _tmp23_;
	gint _tmp23__length1;
	gint _tmp23__length2;
	Model* _tmp24_;
	Pos _tmp25_;
	Model* _tmp26_;
	Pos _tmp27_;
	gint _tmp28_;
	gchar* _tmp29_;
	gchar* _tmp30_;
	const gchar* _tmp31_;
	GtkLabel** _tmp32_;
	gint _tmp32__length1;
	gint _tmp32__length2;
	Model* _tmp33_;
	Pos _tmp34_;
	Model* _tmp35_;
	Pos _tmp36_;
	GtkLabel* _tmp37_;
	Model* _tmp38_;
	gint* _tmp39_;
	gint _tmp39__length1;
	gint _tmp39__length2;
	Model* _tmp40_;
	Pos _tmp41_;
	Model* _tmp42_;
	Pos _tmp43_;
	gint _tmp44_;
	gchar* _tmp45_;
	gchar* _tmp46_;
	const gchar* _tmp47_;
	gchar* _tmp48_;
	gchar* _tmp49_;
	GtkLabel* label1 = NULL;
	Model* _tmp50_;
	gchar* _tmp51_;
	gchar* _tmp52_;
	gchar* _tmp53_;
	gchar* _tmp54_;
	GtkLabel* _tmp55_;
	GtkLabel* _tmp56_;
	GtkListBox* _tmp57_;
#line 37 "../calc.vala"
	g_return_if_fail (self != NULL);
#line 37 "../calc.vala"
	g_return_if_fail (button != NULL);
#line 39 "../calc.vala"
	_tmp0_ = gtk_button_get_label (button);
#line 39 "../calc.vala"
	_tmp1_ = _tmp0_;
#line 39 "../calc.vala"
	_tmp2_ = g_unichar_to_string (string_get_char (_tmp1_, (glong) 1));
#line 39 "../calc.vala"
	_tmp3_ = _tmp2_;
#line 39 "../calc.vala"
	_tmp4_ = atoi (_tmp3_);
#line 39 "../calc.vala"
	_g_free0 (_tmp3_);
#line 39 "../calc.vala"
	a = _tmp4_;
#line 40 "../calc.vala"
	_tmp5_ = g_strdup_printf ("%i", a);
#line 40 "../calc.vala"
	_tmp6_ = _tmp5_;
#line 40 "../calc.vala"
	g_message ("calc.vala:40: %s", _tmp6_);
#line 40 "../calc.vala"
	_g_free0 (_tmp6_);
#line 41 "../calc.vala"
	_tmp7_ = self->priv->model;
#line 41 "../calc.vala"
	model_input (_tmp7_, a);
#line 44 "../calc.vala"
	_tmp8_ = self->priv->labels;
#line 44 "../calc.vala"
	_tmp8__length1 = self->priv->labels_length1;
#line 44 "../calc.vala"
	_tmp8__length2 = self->priv->labels_length2;
#line 44 "../calc.vala"
	_tmp9_ = self->priv->model;
#line 44 "../calc.vala"
	_tmp10_ = _tmp9_->old_pos;
#line 44 "../calc.vala"
	_tmp11_ = self->priv->model;
#line 44 "../calc.vala"
	_tmp12_ = _tmp11_->old_pos;
#line 44 "../calc.vala"
	_tmp13_ = _tmp8_[((_tmp10_.y - 1) * _tmp8__length2) + (_tmp12_.x - 1)];
#line 44 "../calc.vala"
	_tmp14_ = gtk_label_get_label (_tmp13_);
#line 44 "../calc.vala"
	_tmp15_ = _tmp14_;
#line 44 "../calc.vala"
	g_message ("calc.vala:44: %s", _tmp15_);
#line 45 "../calc.vala"
	_tmp16_ = self->priv->labels;
#line 45 "../calc.vala"
	_tmp16__length1 = self->priv->labels_length1;
#line 45 "../calc.vala"
	_tmp16__length2 = self->priv->labels_length2;
#line 45 "../calc.vala"
	_tmp17_ = self->priv->model;
#line 45 "../calc.vala"
	_tmp18_ = _tmp17_->old_pos;
#line 45 "../calc.vala"
	_tmp19_ = self->priv->model;
#line 45 "../calc.vala"
	_tmp20_ = _tmp19_->old_pos;
#line 45 "../calc.vala"
	_tmp21_ = _tmp16_[((_tmp18_.y - 1) * _tmp16__length2) + (_tmp20_.x - 1)];
#line 45 "../calc.vala"
	_tmp22_ = self->priv->model;
#line 45 "../calc.vala"
	_tmp23_ = _tmp22_->z;
#line 45 "../calc.vala"
	_tmp23__length1 = _tmp22_->z_length1;
#line 45 "../calc.vala"
	_tmp23__length2 = _tmp22_->z_length2;
#line 45 "../calc.vala"
	_tmp24_ = self->priv->model;
#line 45 "../calc.vala"
	_tmp25_ = _tmp24_->old_pos;
#line 45 "../calc.vala"
	_tmp26_ = self->priv->model;
#line 45 "../calc.vala"
	_tmp27_ = _tmp26_->old_pos;
#line 45 "../calc.vala"
	_tmp28_ = _tmp23_[((_tmp25_.x - 1) * _tmp23__length2) + (_tmp27_.y - 1)];
#line 45 "../calc.vala"
	_tmp29_ = g_strdup_printf ("%i", _tmp28_);
#line 45 "../calc.vala"
	_tmp30_ = _tmp29_;
#line 45 "../calc.vala"
	_tmp31_ = string_to_string (_tmp30_);
#line 45 "../calc.vala"
	gtk_label_set_markup (_tmp21_, _tmp31_);
#line 45 "../calc.vala"
	_g_free0 (_tmp30_);
#line 48 "../calc.vala"
	_tmp32_ = self->priv->labels;
#line 48 "../calc.vala"
	_tmp32__length1 = self->priv->labels_length1;
#line 48 "../calc.vala"
	_tmp32__length2 = self->priv->labels_length2;
#line 48 "../calc.vala"
	_tmp33_ = self->priv->model;
#line 48 "../calc.vala"
	_tmp34_ = _tmp33_->pos;
#line 48 "../calc.vala"
	_tmp35_ = self->priv->model;
#line 48 "../calc.vala"
	_tmp36_ = _tmp35_->pos;
#line 48 "../calc.vala"
	_tmp37_ = _tmp32_[((_tmp34_.y - 1) * _tmp32__length2) + (_tmp36_.x - 1)];
#line 48 "../calc.vala"
	_tmp38_ = self->priv->model;
#line 48 "../calc.vala"
	_tmp39_ = _tmp38_->z;
#line 48 "../calc.vala"
	_tmp39__length1 = _tmp38_->z_length1;
#line 48 "../calc.vala"
	_tmp39__length2 = _tmp38_->z_length2;
#line 48 "../calc.vala"
	_tmp40_ = self->priv->model;
#line 48 "../calc.vala"
	_tmp41_ = _tmp40_->pos;
#line 48 "../calc.vala"
	_tmp42_ = self->priv->model;
#line 48 "../calc.vala"
	_tmp43_ = _tmp42_->pos;
#line 48 "../calc.vala"
	_tmp44_ = _tmp39_[((_tmp41_.x - 1) * _tmp39__length2) + (_tmp43_.y - 1)];
#line 48 "../calc.vala"
	_tmp45_ = g_strdup_printf ("%i", _tmp44_);
#line 48 "../calc.vala"
	_tmp46_ = _tmp45_;
#line 48 "../calc.vala"
	_tmp47_ = string_to_string (_tmp46_);
#line 48 "../calc.vala"
	_tmp48_ = g_strconcat ("<b><big><big>", _tmp47_, "</big></big></b>", NULL);
#line 48 "../calc.vala"
	_tmp49_ = _tmp48_;
#line 48 "../calc.vala"
	gtk_label_set_markup (_tmp37_, _tmp49_);
#line 48 "../calc.vala"
	_g_free0 (_tmp49_);
#line 48 "../calc.vala"
	_g_free0 (_tmp46_);
#line 50 "../calc.vala"
	_tmp50_ = self->priv->model;
#line 50 "../calc.vala"
	_tmp51_ = g_strdup_printf ("%i", model_output_check (_tmp50_));
#line 50 "../calc.vala"
	_tmp52_ = _tmp51_;
#line 50 "../calc.vala"
	_tmp53_ = g_strconcat ("Выход: ", _tmp52_, NULL);
#line 50 "../calc.vala"
	_tmp54_ = _tmp53_;
#line 50 "../calc.vala"
	_tmp55_ = (GtkLabel*) gtk_label_new (_tmp54_);
#line 50 "../calc.vala"
	g_object_ref_sink (_tmp55_);
#line 50 "../calc.vala"
	_tmp56_ = _tmp55_;
#line 50 "../calc.vala"
	_g_free0 (_tmp54_);
#line 50 "../calc.vala"
	_g_free0 (_tmp52_);
#line 50 "../calc.vala"
	label1 = _tmp56_;
#line 51 "../calc.vala"
	_tmp57_ = self->history;
#line 51 "../calc.vala"
	gtk_list_box_insert (_tmp57_, (GtkWidget*) label1, -1);
#line 52 "../calc.vala"
	gtk_widget_show_all ((GtkWidget*) label1);
#line 37 "../calc.vala"
	_g_object_unref0 (label1);
#line 837 "calc.c"
}

static void
_view_input_gtk_button_clicked (GtkButton* _sender,
                                gpointer self)
{
#line 3 "../calc.vala"
	view_input ((View*) self, _sender);
#line 846 "calc.c"
}

static void
view_class_init (ViewClass * klass,
                 gpointer klass_data)
{
#line 3 "../calc.vala"
	view_parent_class = g_type_class_peek_parent (klass);
#line 3 "../calc.vala"
	g_type_class_adjust_private_offset (klass, &View_private_offset);
#line 3 "../calc.vala"
	G_OBJECT_CLASS (klass)->finalize = view_finalize;
#line 3 "../calc.vala"
	gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (klass), "/org/gavr/calc/calc.ui");
#line 3 "../calc.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "mainGrid", FALSE, View_private_offset + G_STRUCT_OFFSET (ViewPrivate, mainGrid));
#line 3 "../calc.vala"
	gtk_widget_class_bind_template_child_full (GTK_WIDGET_CLASS (klass), "history", FALSE, G_STRUCT_OFFSET (View, history));
#line 3 "../calc.vala"
	gtk_widget_class_bind_template_callback_full (GTK_WIDGET_CLASS (klass), "input", G_CALLBACK(_view_input_gtk_button_clicked));
#line 867 "calc.c"
}

static void
view_instance_init (View * self,
                    gpointer klass)
{
#line 3 "../calc.vala"
	self->priv = view_get_instance_private (self);
#line 3 "../calc.vala"
	gtk_widget_init_template (GTK_WIDGET (self));
#line 878 "calc.c"
}

static void
view_finalize (GObject * obj)
{
	View * self;
#line 3 "../calc.vala"
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, TYPE_VIEW, View);
#line 6 "../calc.vala"
	_g_object_unref0 (self->priv->mainGrid);
#line 9 "../calc.vala"
	_g_object_unref0 (self->history);
#line 11 "../calc.vala"
	_g_object_unref0 (self->priv->lg);
#line 12 "../calc.vala"
	_g_object_unref0 (self->priv->model);
#line 13 "../calc.vala"
	self->priv->labels = (_vala_array_free (self->priv->labels, self->priv->labels_length1 * self->priv->labels_length2, (GDestroyNotify) g_object_unref), NULL);
#line 15 "../calc.vala"
	_g_free0 (self->priv->old_l_txt);
#line 16 "../calc.vala"
	_g_free0 (self->priv->l_txt);
#line 3 "../calc.vala"
	G_OBJECT_CLASS (view_parent_class)->finalize (obj);
#line 903 "calc.c"
}

GType
view_get_type (void)
{
	static volatile gsize view_type_id__volatile = 0;
	if (g_once_init_enter (&view_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (ViewClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) view_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (View), 0, (GInstanceInitFunc) view_instance_init, NULL };
		GType view_type_id;
		view_type_id = g_type_register_static (gtk_window_get_type (), "View", &g_define_type_info, 0);
		View_private_offset = g_type_add_instance_private (view_type_id, sizeof (ViewPrivate));
		g_once_init_leave (&view_type_id__volatile, view_type_id);
	}
	return view_type_id__volatile;
}

void
_vala_main (gchar** args,
            gint args_length1)
{
	View* widget = NULL;
	View* _tmp0_;
#line 57 "../calc.vala"
	gtk_init ((gint*) (&args_length1), &args);
#line 58 "../calc.vala"
	_tmp0_ = view_new ();
#line 58 "../calc.vala"
	g_object_ref_sink (_tmp0_);
#line 58 "../calc.vala"
	widget = _tmp0_;
#line 60 "../calc.vala"
	gtk_main ();
#line 56 "../calc.vala"
	_g_object_unref0 (widget);
#line 938 "calc.c"
}

int
main (int argc,
      char ** argv)
{
#line 56 "../calc.vala"
	_vala_main (argv, argc);
#line 56 "../calc.vala"
	return 0;
#line 949 "calc.c"
}

static void
_vala_array_destroy (gpointer array,
                     gint array_length,
                     GDestroyNotify destroy_func)
{
	if ((array != NULL) && (destroy_func != NULL)) {
		int i;
		for (i = 0; i < array_length; i = i + 1) {
			if (((gpointer*) array)[i] != NULL) {
				destroy_func (((gpointer*) array)[i]);
			}
		}
	}
}

static void
_vala_array_free (gpointer array,
                  gint array_length,
                  GDestroyNotify destroy_func)
{
	_vala_array_destroy (array, array_length, destroy_func);
	g_free (array);
}

