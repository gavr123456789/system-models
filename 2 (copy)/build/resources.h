#ifndef __RESOURCE_calc_resources_H__
#define __RESOURCE_calc_resources_H__

#include <gio/gio.h>

G_GNUC_INTERNAL GResource *calc_resources_get_resource (void);
#endif
